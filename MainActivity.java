package com.example.leoscalc;

import android.app.Activity;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends Activity {
	TextView display;
	private boolean isEmpty = true;
	private float buffer;
	private short operation = 10;
	private int val;
	private MediaPlayer clickSound;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_layout);
		display = (TextView) findViewById(R.id.textView1);
		display.setText("0");
		clickSound = MediaPlayer.create(this, R.raw.click1);
		
	}

	// listening for number pressing
	public void num_Clicked(View view) {
		Button button = (Button) view;
			clickSound.reset();
			// ... I guess it's not a very good temper,
			// but had troubles with playing of short sound repeatedly,
			// So used this temporary solution 
			clickSound = MediaPlayer.create(this, R.raw.click1);
			clickSound.start();
		
		//defining number of digits
		if (display.length() > 11) 
			return;
		
		if (button.getText().toString().equals(".") && (display.getText().toString().contains("."))) 
				return;
		
		if (isEmpty) {
			
			if (button.getText().toString().equals("0"))
				return;
			
			if (button.getText().toString().equals(".")) {
				display.setText("0.");
				isEmpty = false;
				return;
			}
			
			display.setText(button.getText());
			isEmpty = false;
		}
		
		else {
			display.append(button.getText());
		}
	}
	
	// listening for operation buttons pressing
	public void op_Clicked(View view) {
		
		Button button = (Button) view;
		clickSound.reset();
		clickSound = MediaPlayer.create(this, R.raw.click1);
		clickSound.start();
		
		switch (operation) {
		
		// error
		case 9:
			
			display.setText("0");
			break;
		
		// new calculation
		case 10:
			
		buffer = Float.parseFloat(display.getText().toString());
		break;
		
		// +
		case 0:
			if (buffer + Float.parseFloat(display.getText().toString()) > Float.MAX_VALUE) {
				
				display.setText("Err");
				operation = 9;
				buffer = 0;
				isEmpty = true;
				return;
			}
			
			buffer += Float.parseFloat(display.getText().toString());
		break;
		
		// -
		case 1:
			if (buffer - Float.parseFloat(display.getText().toString()) < Float.MIN_VALUE) {
				
				display.setText("Err");
				operation = 9;
				buffer = 0;
				isEmpty = true;
				return;
			}
			
			buffer -= Float.parseFloat(display.getText().toString());
		break;
		
		// *
		case 2:

			if (buffer * Float.parseFloat(display.getText().toString()) > Float.MAX_VALUE) {
				
				display.setText("Err");
				operation = 9;
				buffer = 0;
				isEmpty = true;
				return;
			}
			
			buffer *= Float.parseFloat(display.getText().toString());
		break;
		
		// /
		case 3:
			
			if (buffer / Float.parseFloat(display.getText().toString()) < Float.MIN_VALUE) {
				
				display.setText("Err");
				operation = 9;
				buffer = 0;
				isEmpty = true;
				return;
			}
			
			buffer /= Float.parseFloat(display.getText().toString());
		break;
		
		case 4:
			buffer = Float.parseFloat(display.getText().toString());
			operation = 10;
		break;
	
	}
		
		// getting the buttons info.
		// without using of id-s
		if (button.getText().toString().equals("+"))
			operation = 0;
		else if (button.getText().toString().equals("-"))
		operation = 1;
		
		else if (button.getText().toString().equals("*"))
			operation = 2;
		else if (button.getText().toString().equals("/"))
			operation = 3;
		else if (button.getText().toString().equals("="))
			operation = 4;
		
		val = Integer.parseInt(Float.toString(buffer).substring(0, Float.toString(buffer).lastIndexOf(".")));
		
		if (val == buffer) {
			display.setText(Integer.toString(val));
		}
		
		else {
			display.setText(Float.toString(buffer));
		}
		isEmpty = true;
	}
	
	// reading for other function pressing
	public void func_Clicked(View view) {
	
		Button button = (Button) view;
		clickSound.reset();
		clickSound = MediaPlayer.create(this, R.raw.click1);
		clickSound.start();
		
		// backspace (C)
		if (button.getText().toString().equals("C")) {
			if (display.getText().toString().length() == 1) {
				display.setText("0");
				return;
			}
			display.setText(display.getText().toString().substring(0, display.getText().toString().length()-1));
			buffer = Float.parseFloat(display.getText().toString());
		}
		
		// - +
		else if (button.getText().toString().equals("- +")) {
			
			
			if (!display.getText().toString().contains("-")) {
				display.setText("-" + display.getText());
			}
			
			else {
				display.setText(display.getText().toString().replace("-", ""));
			}
		}
		
		// reset current operation
		else if (button.getText().toString().equals("AC")) {
			
			display.setText("0");
			buffer = 0;
			isEmpty = true;
			operation = 10;
			
		}
		
		// % calculation
		else if (button.getText().toString().equals("%")) {
			
			buffer = Float.parseFloat(display.getText().toString()) / 100;
			display.setText(Float.toString(buffer)); 
			isEmpty = true;	
			
		}
	}
}
